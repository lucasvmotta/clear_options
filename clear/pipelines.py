from scrapy.exceptions import DropItem

class SaveStockData(object):
	def process_item(self, item, spider):
		try:
			spider.extracted.append(item)
		except:
			raise DropItem("Invalid item")
	def close_spider(self,spider):
		for item in spider.extracted:
			print item