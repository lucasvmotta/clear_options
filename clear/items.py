# -*- coding: utf-8 -*-
import scrapy

class OptionItem(scrapy.Item):
	opcao = scrapy.Field
	tipo = scrapy.Field
	strike = scrapy.Field
	max_loss = scrapy.Field
	max_profit = scrapy.Field