#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
import json
from decouple import config
DEBUG = config('LUCAS', default=False)

class ClearSpider(scrapy.Spider):
	name = "clear"
	# def __init__(self, cpf='',nasc='',passw='',stock=''):
	cpf = config('cpf', default=False)
	nasc = config('nasc', default=False)
	passw = config('passw', default=False)
	stock = config('stock', default=False)

	#Guarda a lista de dados extraidos
	extracted = []
	#Dicionario para traduzir tipo e opcao
	exerciseStyleDict = {0:'Undefined',1:'American',2:'European'}
	#Dicionario para traduzir put ou call
	putOrCallDict = {1:'Put',2:'Call'}
	base_url = 'https://www.clear.com.br/pit'

	#Request inicial, pagina principal da clear
	def start_requests(self):
		try:
			yield scrapy.Request(url='%s/signin?controller=SignIn' % self.base_url, callback=self.execute_login)
		except Exception, e:
			print('Error geral: %s : %s' % (self.name,str(e)))

	#Efetua login na clear
	def execute_login(self, response):
		return scrapy.FormRequest(url='%s/signin/Do?controller=SignIn' % self.base_url,
								formdata={
										'identificationNumber':self.cpf,
										'password':self.passw,
										'dob':self.nasc},
								callback=self.get_options_names_from_stock)

	#Extrai todas opcoes do papel selecionado
	def get_options_names_from_stock(self,response):
		return scrapy.Request(url='%s/Options/leg/GetItems?orderStrategy=PutOrCall&underlying=%s&action=New' % (self.base_url,self.stock),
							callback=self.get_jsons_of_options)


	#Extrai o json de cada uma das puts ou calls
	def get_jsons_of_options(self,response):
		try:
			for item in json.loads(response.body_as_unicode()):
				yield scrapy.Request(url='%s/Options/docketdata/GetItems?orderStrategy=PutOrCall&symbol=%s&rolloverSymbol=&side=Buy' % (self.base_url,item.get('Symbol')),
									callback=self.extract_jsons_data)
		except:
			print ('########################################')
			print ('Voce lembrou de alterar o arquivo .env?')
			print ('########################################')

	def extract_jsons_data(self,response):
		try:
			jsonResponse = json.loads(response.body_as_unicode())
			yield { 'opcao':jsonResponse.get('Strategy').get('Legs')[0].get('Instrument').get('Symbol'),
				'tipo':self.putOrCallDict.get(jsonResponse.get('Strategy').get('Legs')[0].get('Instrument').get('PutOrCall')),
				'strike':jsonResponse.get('Strategy').get('Legs')[0].get('Strike'),
				'max_loss':jsonResponse.get('PnL').get('MaximumLoss'),
				'max_profit':jsonResponse.get('PnL').get('MaximumProfit')}
		except:
			#Alguns strikes dao erro ao abrir na pagina da clear
			pass
